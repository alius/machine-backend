from flask import Flask
from werkzeug.routing import NumberConverter, ValidationError

from database import pgsql
from database import database

from app.facade import Facade
from app.web.main import web_blueprint
from app.cameras.main import cameras_blueprint


class NegativeFloatConverter(NumberConverter):
    regex = r'\-?\d+\.\d+'
    num_convert = float
 
    def __init__(self, map, min=None, max=None):
        NumberConverter.__init__(self, map, 0, min, max)

app = Flask(__name__)
app.config.from_object('config')
app.url_map.converters['float'] = NegativeFloatConverter

app.register_blueprint(cameras_blueprint)
app.register_blueprint(web_blueprint)

pg = pgsql.Postgresql(app.config["DATABASE_DSN"])
app.facade = Facade(database.Database(pg))
