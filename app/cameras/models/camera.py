class Camera:
    type = "unknown"
    description = ""
    bearing = None
    city = ""
    zip = ""
    street = ""
    country = ""
    latitude = None
    longitude = None

    def __init__(self, data):
        if 'type' in data:
            self.type = data['type']
        if 'description' in data:
            self.description = data['description']
        if 'bearing' in data:
            self.bearing = data['bearing']
        if 'city' in data:
            self.city = data['city']
        if 'zip' in data:
            self.zip = data['zip']
        if 'street' in data:
            self.street = data['street']
        if 'country' in data:
            self.country = data['country']
        if all(k in data for k in ('latitude', 'longitude')):
            self.latitude = data['latitude']
            self.longitude = data['longitude']

    def __getitem__(self, i):
        if i in self.__dict__ and self.__dict__[i] is not None and self.__dict__[i] is not "":
            return self.__dict__[i]

    def __delitem__(self, i):
        if i in self.__dict__:
            if type(self.__dict__[i]).__name__ == 'str':
                self.__dict__[i] = ''
            else:
                self.__dict__[i] = None

    def __iter__(self):
        for k,v in self.__dict__.items():
            yield k

    def __str__(self):
        return "%s[%s] located at %s,%s" % (self.type, self.bearing, self.latitude, self.longitude)

    def toDict(self):
        return {"type":self.type, "description":self.description, "bearing":self.bearing, "city":self.city,
                "zip":self.zip, "street":self.street, "country":self.country, "latitude":self.latitude, "longitude":self.longitude}
