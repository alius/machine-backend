import json

from flask import request
from flask import Response
from flask import Blueprint
from flask import current_app

from app.cameras.models.camera import Camera

cameras_blueprint = Blueprint("cameras", __name__, template_folder="templates")

@cameras_blueprint.route('/cameras/<float:lat>/<float:lon>', defaults={'radius':500}, methods=['GET'])
@cameras_blueprint.route('/cameras/<float:lat>/<float:lon>/<int:radius>', methods=['GET'])
def get_cameras(lat, lon, radius):
    """ Find all the nearby cameras withing the specific radius of the location provided
        lat: latitude
        lon: longitude
        radius: optional radius in meters, defaults to 500 """
    cameras = current_app.facade.getCameras(lat, lon, radius)
    if len(cameras) > 0:
        return Response(json.dumps([c.toDict() for c in cameras]), mimetype='application/json')
    else:
        r = Response(json.dumps("Nothing found"), mimetype='application/json')
        r.status_code = 404 
        return r

@cameras_blueprint.route('/camera/types', methods=['GET'])
def get_types():
    types = current_app.facade.getCameraTypes()
    if len(types) > 0:
        return Response(json.dumps(types), mimetype='application/json')
    else:
        r = Response(json.dumps("Nothing found"), mimetype='application/json')
        r.status_code = 404 
        return r

@cameras_blueprint.route('/camera', methods=['POST'])
def add_camera():
    if "latitude" not in request.form:
        r = Response(json.dumps("Missing latitude"), mimetype='application/json')
        r.status_code = 400 
        return r
    elif "longitude" not in request.form:
        r = Response(json.dumps("Missing longitude"), mimetype='application/json')
        r.status_code = 400 
        return r
    elif "type" not in request.form:
        r = Response(json.dumps("Missing type"), mimetype='application/json')
        r.status_code = 400 
        return r

    c = Camera({"type":request.form["type"], "latitude":request.form["latitude"], "longitude":request.form["longitude"]})
    if "description" in request.form: c.description = request.form["description"]
    if "bearing" in request.form: c.bearing = request.form["bearing"]
    if "city" in request.form: c.city = request.form["city"]
    if "zip" in request.form: c.zip = request.form["zip"]
    if "street" in request.form: c.street = request.form["street"]
    if "country" in request.form: c.country = request.form["country"]

    if current_app.facade.addCamera(c):
        r = Response(json.dumps("Camera registered"), mimetype="application/json")
        r.status_code = 201 
        return r
    else:
        r = Response(json.dumps("Error saving camera info"), mimetype='application/json')
        r.status_code = 500 
        return r
