from adapter import Adapter

class Database:

    def __init__(self, db):
        if not isinstance(db, Adapter):
            raise TypeError("Invalid interface passed")

        self.db = db

    def getAdapter(self):
        return self.db

    def findWithRadius(self, where, latitude, longitude, radius):
        if not self.db:
            raise RuntimeError("Invalid database driver")

        try:
            return self.db.geoFindWithin(where, latitude, longitude, radius)
        except AttributeError, e:
            raise e

    def store(self, where, model):
        if not self.db:
            raise RuntimeError("Invalid database driver")

        if "latitude" in model and "longitude" in model:
            try:
                return self.db.geoStore(where, model)
            except AttributeError, e:
                raise e
        else:
            return False

    def fetchAll(self, where):
        if not self.db:
            raise RuntimeError("Invalid database driver")

        try:
            return self.db.findAll(where, 1, 1)
        except AttributeError, e:
            raise e
