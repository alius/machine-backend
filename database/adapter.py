class Adapter:
    """ Describes the API for the database interface
    All geo functions assume that the table has 'location' field """

    def geoFindWithin(self, radius):
        """ Find the objects withing the specified radius """
        raise NotImplementedError

    def geoFindClosest(self, latitude, longitude, maximum):
        """ Find the X closest objects to the specified location """
        raise NotImplementedError

    def geoStore(self):
        """ Store the GEO object in the database """
        raise NotImplementedError

