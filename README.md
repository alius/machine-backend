# README #

The Machine project. An attempt to map all CCTV cameras around the world and have fun in doing so. This repository is a backend code for doing it.

It was built during the personal hack day project during a weekend. Consider this a proof of concept rather than a solid and secure implementation of the idea.

### How do I get set up? ###

It is a Flask app which uses PostgreSQL as a storage database.