from app.cameras.models.camera import Camera

class Facade:

    def __init__(self, db):
        self.db = db

    def getCameras(self, latitude, longitude, radius):
        if self.db:
            raw = self.db.findWithRadius("cameras", latitude, longitude, radius)
            if raw is not None:
                return [Camera(c) for c in raw]
            return []

    def addCamera(self, c):
        if self.db:
            return self.db.store("cameras", c.toDict())
        return False

    def getCameraTypes(self):
        if self.db:
            types = self.db.fetchAll("camera_types")
            if types is not None:
                return [t[1] for t in types]
            return []
