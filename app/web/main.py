from flask import Blueprint

web_blueprint = Blueprint("web", __name__, template_folder="templates")

@web_blueprint.route('/')
def hello_world():
    return "I'm watching you" 
