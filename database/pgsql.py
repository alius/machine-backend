import psycopg2
import psycopg2.extras

from adapter import Adapter

class Postgresql(Adapter):

    def __init__(self, dsn):
        try:
            self.connection = psycopg2.connect(dsn)
            if self.connection:
                self.cursor = self.connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
            else:
                self.cursor = None
        except psycopg2.DatabaseError, e:
            self.cursor = None

    def geoFindWithin(self, where, latitude, longitude, radius):
        """ Find the objects withing the specified radius
        where - name of the table
        latitude - float
        longitude - float
        radius - in meters
        """
        if self.cursor:
            self.cursor.execute(
                """SELECT *, ST_Y(location) AS latitude, ST_X(location) AS longitude
                    FROM {0}, (SELECT ST_MakePoint(%s,%s)::geography AS poi) AS poi
                    WHERE ST_DWithin(location, poi, %s) LIMIT 500;""".format(where),
                (longitude, latitude, radius))

            return self.cursor.fetchall()

        return False

    def geoStore(self, where, model):
        """ Store the GEO object in the database """
        if self.cursor:
            if "latitude" not in model or "longitude" not in model:
                return False

            lat = float(model["latitude"])
            lon = float(model["longitude"])

            del model["latitude"], model["longitude"]

            queryStart = "INSERT INTO %s(" % where
            queryEnd = " VALUES ("
            for k in model.keys():
                queryStart += "%s, " % k
                queryEnd += "%("+str(k)+")s, "

            queryStart += "location)"
            queryEnd += "ST_GeomFromText('POINT(%s %s)', 4326));" % (lon, lat)

            query = queryStart + queryEnd

            self.cursor.execute(query, model)
            self.cursor.connection.commit()

            return True
        return False

    def findAll(self, where, what, search):
        """ Find all the  items from the table
            where - name of the table
            what - name of the field to use for search
            search - search criteria """
        if self.cursor:
            self.cursor.execute(
                """SELECT * FROM {0} WHERE %s = %s;""".format(where),
                (what, search))

            return self.cursor.fetchall()
        return False
